package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldService {

	@RequestMapping("/hello")
	String hello(@RequestParam(value="name", defaultValue = "Manolo") String name) {
		return "Hello " + name;
	}

}

